/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_di.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/01 12:13:09 by apyvovar          #+#    #+#             */
/*   Updated: 2017/01/21 12:15:19 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

int			print_int(va_list ap, char *fmt, int width, int prec)
{
	char	*result;
	int		count;

	result = save_int(ap, fmt, fmt_isnumarg(fmt));
	if (prec == 0 && result[0] == '0')
		result[0] = '\0';
	apply_flprec_int(prec, fmt, &result, ft_strlen(result));
	apply_flwid_int(width, fmt, &result, prec);
	count = ft_strlen(result);
	ft_putstr(result);
	free(result);
	return (count);
}

char		*save_int(va_list ap, char *fmt, int num_arg)
{
	intmax_t	nbr;

	if (num_arg)
		nbr = getnumarg(ap, ft_atoi(fmt + 1), fmt);
	else
		nbr = va_arg(ap, intmax_t);
	if (CFMT('j'))
		;
	else if (ACFMT('z'))
		nbr = INTTYPE_Z(nbr);
	else if (CFMT('l') || CFMT('D'))
		nbr = (ft_strstr(fmt, "ll")) ? INTTYPE_LL(nbr) : INTTYPE_L(nbr);
	else if (CFMT('h'))
		nbr = (ft_strstr(fmt, "hh")) ? INTTYPE_HH(nbr) : INTTYPE_H(nbr);
	else
		nbr = INTTYPE_I(nbr);
	return (int_itoa(nbr));
}

char		*int_itoa(intmax_t n)
{
	char		*result;
	int			size;
	intmax_t	i;

	i = n;
	size = (n < 0) ? 2 : 1;
	while (i /= 10)
		size++;
	result = (char *)malloc(sizeof(char) * size + 1);
	if (!result)
		return (NULL);
	if (n < 0)
		result[0] = '-';
	result[size] = '\0';
	while (size--)
	{
		result[size] = ABS(n % 10) + '0';
		n /= 10;
		if (n == 0)
			break ;
	}
	return (result);
}

intmax_t	getnumarg(va_list ap, int n, char *fmt)
{
	intmax_t	result;

	if ((ACFMT('f') && fmt_isnumarg(fmt) && ft_atoi(&fmt[1]) < n) \
		|| (ACFMT('f') && !fmt_isnumarg(fmt)))
	{
		CFMT('f') ? va_arg(ap, double) : va_arg(ap, long double);
		n--;
	}
	while (--n > 0)
		va_arg(ap, intmax_t);
	result = va_arg(ap, intmax_t);
	return (result);
}

int			getnextarg_int(va_list ap, int len, char *fmt)
{
	int		result;
	va_list	temp;

	va_copy(temp, ap);
	while (len >= 0)
	{
		if (fmt[len] == '*' && !fmt_isnumarg(&fmt[len]))
			result = va_arg(temp, int);
		len--;
	}
	va_end(temp);
	return (result);
}
