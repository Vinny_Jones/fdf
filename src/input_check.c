/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   input_check.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 21:02:34 by apyvovar          #+#    #+#             */
/*   Updated: 2017/03/28 16:33:43 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		input_check(t_file *input, int width)
{
	while (input)
	{
		if (get_width(input->line) != width)
			return (0);
		input = input->next;
	}
	return (1);
}

int		get_height(t_file *input)
{
	int	result;

	result = 0;
	while (input)
	{
		result++;
		input = input->next;
	}
	return (result);
}

int		get_width(char *line)
{
	int	result;

	if (!line)
		return (0);
	result = 0;
	while (*line == ' ')
		line++;
	while (*line)
	{
		if ((line = is_int(line)))
			result++;
		else
			return (0);
		if (*line == ',')
			line += check_color_format(line + 1);
		while (*line == ' ')
			line++;
	}
	if (!result)
		fmt_print("Input data error: Wrong line length.\n", 10, 2, 0);
	return (result);
}

char	*is_int(char *str)
{
	int	i;
	int	j;
	int	sign;
	int	nbr;

	nbr = ft_atoi(str);
	sign = (*str == '-') ? -1 : 1;
	if (*str == '-' || *str == '+')
		str++;
	if (!ft_isdigit(*str))
		return (NULL);
	i = 0;
	while (ft_isdigit(str[i]))
		i++;
	j = i;
	while (--j >= 0)
	{
		if (nbr % 10 - (str[j] - '0') * sign)
			return (NULL);
		nbr /= 10;
	}
	return (&str[i]);
}

int		check_color_format(char *line)
{
	char	*temp;

	temp = line;
	if (ft_strncmp(line, "0x", 2))
		return (0);
	line = line + 2;
	if (!ft_ishex(*line))
		return (0);
	while (ft_ishex(*line))
		line++;
	if ((!*line || *line == ' ') && line - temp <= 8)
		return (line - temp + 1);
	return (0);
}
