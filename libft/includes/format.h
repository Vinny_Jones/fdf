/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   format.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/28 12:22:18 by apyvovar          #+#    #+#             */
/*   Updated: 2017/03/11 23:44:42 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FORMAT_H
# define FORMAT_H

# define FMT_BLACK "\033[30m"
# define FMT_RED "\033[31m"
# define FMT_GREEN "\033[32m"
# define FMT_YELLOW "\033[33m"
# define FMT_BLUE "\033[34m"
# define FMT_MAGENTA "\033[35m"
# define FMT_CYAN "\033[36m"
# define FMT_WHITE "\033[37m"

# define FMT_BG_BLACK "\033[40m"
# define FMT_BG_RED "\033[41m"
# define FMT_BG_GREEN "\033[42m"
# define FMT_BG_YELLOW "\033[43m"
# define FMT_BG_BLUE "\033[44m"
# define FMT_BG_MAGENTA "\033[45m"
# define FMT_BG_CYAN "\033[46m"
# define FMT_BG_WHITE "\033[47m"

# define FMT_BOLD "\033[1m"
# define FMT_ULINE "\033[4m"

# define FMT_OFF "\033[0m"

int		fmt_print(char *text, int bold_line, int color, int bg);
void	fmt_init_color(char **color, char **bg_color);

#endif
