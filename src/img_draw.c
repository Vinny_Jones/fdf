/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   img_draw.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/23 11:09:08 by apyvovar          #+#    #+#             */
/*   Updated: 2017/03/28 18:04:15 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	draw_image(t_win *win)
{
	int		x_shift;
	int		y_shift;
	int		i;
	int		j;

	x_shift = (SIDE - win->max_x - win->min_x) / 2 + win->x_shift;
	y_shift = (SIDE - win->max_y - win->min_y) / 2 + win->y_shift;
	i = -1;
	while (win->image[++i])
	{
		j = -1;
		while (win->image[i][++j])
		{
			win->image[i][j]->px += x_shift;
			win->image[i][j]->py += y_shift;
			if (i > 0)
				draw_line(win, win->image[i - 1][j], win->image[i][j]);
			if (j > 0)
				draw_line(win, win->image[i][j - 1], win->image[i][j]);
			if (win->diagonal_flag && i > 0 && win->image[i][j + 1])
				draw_line(win, win->image[i][j], win->image[i - 1][j + 1]);
		}
	}
}

void	draw_line(t_win *win, t_point *start, t_point *end)
{
	t_draw	*tmp;
	int		i;
	double	x;
	double	y;

	if (!check_line(start, end) || !(tmp = init_draw_line(win, start, end)))
		return ;
	i = tmp->l;
	x = (double)tmp->sx;
	y = (double)tmp->sy;
	while (--i)
	{
		x = x + tmp->dx;
		y = y + tmp->dy;
		if ((int)round(x) < SIDE && (int)round(y) < SIDE)
			draw_pixel(win, (int)round(x), (int)round(y), \
				mix_colors(tmp, &tmp->s_color, &tmp->e_color, i));
	}
	free(tmp);
}

t_draw	*init_draw_line(t_win *win, t_point *start, t_point *end)
{
	t_draw	*tmp;

	draw_pixel(win, start->px, start->py, start->color);
	draw_pixel(win, end->px, end->py, end->color);
	if (!(tmp = (t_draw *)malloc(sizeof(t_draw))))
		return (NULL);
	tmp->sx = start->px;
	tmp->sy = start->py;
	tmp->ex = end->px;
	tmp->ey = end->py;
	tmp->s_color = (start->color < 0) ? win->default_color : start->color;
	tmp->e_color = (end->color < 0) ? win->default_color : end->color;
	tmp->l = MAX(ABS(tmp->ex - tmp->sx), ABS(tmp->ey - tmp->sy));
	if (tmp->l <= 1)
	{
		free(tmp);
		return (NULL);
	}
	tmp->dx = (tmp->ex - tmp->sx) / (double)tmp->l;
	tmp->dy = (tmp->ey - tmp->sy) / (double)tmp->l;
	return (tmp);
}

int		check_line(t_point *start, t_point *end)
{
	if ((start->px < 0 && end->px < 0) || (start->px > SIDE && end->px > SIDE))
		return (0);
	if ((start->py < 0 && end->py < 0) || (start->py > SIDE && end->py > SIDE))
		return (0);
	return (1);
}

void	draw_pixel(t_win *win, int x, int y, int color)
{
	char	*img;
	int		*temp;
	int		bits_pp;
	int		line;

	if (x < 0 || y < 0 || x >= SIDE || y >= SIDE)
		return ;
	if (color < 0)
		color = win->default_color;
	img = win->mlx_img_addr;
	bits_pp = win->bits_pp;
	line = win->size_line;
	img = img + y * line + x * bits_pp / 8;
	temp = (int *)img;
	*temp = color;
}
