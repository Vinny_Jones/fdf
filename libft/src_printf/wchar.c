/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wchar.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/06 01:12:14 by apyvovar          #+#    #+#             */
/*   Updated: 2017/01/20 14:02:33 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

int	write_wchar(int ch, char *fmt, int width)
{
	unsigned char	wchar[5];
	int				bytes;
	int				i;
	char			filler;

	filler = isnulflag(fmt) ? '0' : ' ';
	i = 0;
	while (i < 5)
		wchar[i++] = 0;
	bytes = save_wchar(wchar, ch);
	i = 0;
	while (!CFMT('-') && i++ < width - bytes)
		write(1, &filler, 1);
	write(1, wchar, bytes);
	i = 0;
	while ((CFMT('-') || width < 0) && i++ < ABS(width) - bytes)
		write(1, " ", 1);
	return (MAX(bytes, ABS(width)));
}

int	save_wchar(unsigned char *wch, int ch)
{
	int	oct;
	int	itr;
	int	i;
	int add[3];

	add[0] = 300;
	add[1] = 340;
	add[2] = 360;
	itr = 1;
	if (ch <= 127)
		wch[0] = ch;
	else if (ch <= 2047)
		itr = 2;
	else if (ch <= 65535)
		itr = 3;
	else if (ch <= 1114111)
		itr = 4;
	i = 0;
	oct = inttooct(ch);
	while (i++ < itr && itr > 1)
	{
		wch[itr - i] = octtoint(oct % 100 + ((i == itr) ? add[itr - 2] : 200));
		oct = oct / 100;
	}
	return ((ch > 1114111 || ch < 0) ? 0 : itr);
}

int	inttooct(int nb)
{
	char	*tmp;
	int		res;

	tmp = uint_itoa_base(nb, "", 8);
	res = ft_atoi(tmp);
	free(tmp);
	return (res);
}

int	octtoint(int nb)
{
	int	res;
	int	mod;

	res = 0;
	mod = 1;
	while (nb)
	{
		res = res + (nb % 10) * mod;
		nb = nb / 10;
		mod = mod * 8;
	}
	return (res);
}
