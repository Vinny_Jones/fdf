# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/02/18 21:15:16 by apyvovar          #+#    #+#              #
#    Updated: 2017/03/28 18:35:11 by apyvovar         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

FLAGS = -Wall -Wextra -Werror
LIBDIR = libft/
LIB = $(LIBDIR)libft.a
CC = gcc $(FLAGS)
NAME = fdf
INC = includes
SRC_DIR = src/
SRC_FILES = main.c input_read.c input_check.c img_save.c projection.c \
	key_events.c img_actions.c img_actions2.c img_print.c img_draw.c \
	draw_tools.c clean.c
SRC = $(addprefix $(SRC_DIR), $(SRC_FILES))
OBJ = $(SRC:.c=.o)

all: $(NAME)

$(NAME): $(OBJ) $(INC)/fdf.h
	make -C $(LIBDIR)
	$(CC) $(LIB) -lmlx -framework OpenGL -framework AppKit $(OBJ) -o $@

.c.o: $(SRC)
	$(CC) -I $(INC) -I $(LIBDIR)$(INC) -c $^ -o $@

clean:
	make -C $(LIBDIR) clean
	/bin/rm -f $(OBJ)

fclean: clean
	/bin/rm -f $(LIB)
	/bin/rm -f $(NAME)

re: fclean all
