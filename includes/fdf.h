/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/10 14:30:19 by apyvovar          #+#    #+#             */
/*   Updated: 2017/03/28 18:03:32 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H
# include "libft.h"
# include "mlx.h"
# include <fcntl.h>
# include <math.h>
# define PI 3.1415926536
# define ROTATE_DEFAULT_X -3.316125575
# define ROTATE_DEFAULT_Y -5.410520675
# define ROTATE_DEFAULT_Z -1.919862175
# define ROTATE_ANGLE 0.174532925
# define ROTATE_DEFAULT_ISO_X -1.57079633
# define ROTATE_DEFAULT_ISO_Y 1.9106468396
# define SIDE 960
# define MAX_IMG_SIDE 4000.0
# define MIN_IMG_SIDE 150.0
# define Z_INC 0.1
# define ZOOM_K 0.90

/*
** Saving input data in list line by line
*/
typedef struct	s_file
{
	char			*line;
	struct s_file	*next;
}				t_file;
/*
** Image pixel + projection coords
*/
typedef struct	s_point
{
	double	x;
	double	y;
	double	z;
	double	x_default;
	double	y_default;
	double	z_default;
	int		px;
	int		py;
	int		color;
}				t_point;
/*
** Main structure
*/
typedef struct	s_window
{
	void		*mlx;
	void		*mlx_win;
	void		*mlx_img;
	char		*mlx_img_addr;
	int			bits_pp;
	int			size_line;
	int			endian;
	int			proj_type;
	int			controls_flag;
	int			adjust_flag;
	double		rot_x;
	double		rot_y;
	double		rot_z;
	t_point		***image;
	int			default_color;
	int			diagonal_flag;
	int			x_shift;
	int			y_shift;
	int			max_x;
	int			min_x;
	int			max_y;
	int			min_y;
}				t_win;
/*
** Used for calculations while drawing lines
*/
typedef struct	s_draw
{
	int		sx;
	int		sy;
	int		ex;
	int		ey;
	int		s_color;
	int		e_color;
	int		l;
	double	dx;
	double	dy;
}				t_draw;

/*
** input_read.c
*/
void			parse_input(const char *filename);
t_win			*win_init(t_point ***image_map);
t_file			*save_file(const char *filename);
void			proceed_image(t_win *win);
/*
** input_check.c
*/
int				input_check(t_file *input, int width);
int				get_height(t_file *input);
int				get_width(char *line);
char			*is_int(char *str);
int				check_color_format(char *line);
/*
** img_save.c
*/
t_point			***create_image_map(t_file *input, int width, int height);
t_point			**set_image_line(char *input_line, int line_num, int width);
t_point			*set_image_pixel(char *line, int x, int y);
int				get_color(char *line);
/*
** projection.c
*/
void			make_projection(t_win *win);
void			proj_parallel(t_win *win, int width, int height);
void			proj_iso(t_win *win, int width, int height);
void			change_projection(t_win *win);
void			set_minmax(t_win *win, int width, int height);
/*
** key_events.c
*/
int				keycode_mgmt(int keycode, void *par);
int				keycode_mgmt2(int keycode, t_win	*win);
int				mouse_mgmt(int button, int x, int y, void *par);
void			discard_changes(t_win *win);
void			set_default_angle(t_win *win);
/*
** img_actions.c
*/
void			change_height(t_win *win, double n);
double			get_max_z(t_win *win);
void			zoom(t_win *win, double k, int printflag);
void			diagonal_print(t_win *win);
/*
** img_actions2.c
*/
void			rotate_x(t_win *win, int n);
void			rotate_y(t_win *win, int n);
void			rotate_z(t_win *win, int n);
double			get_angle_iso(double current, int n);
void			shift_img(t_win *win, int x_shift, int y_shift);
/*
** img_print.c
*/
void			print_image(t_win *win);
void			print_info(t_win *win);
void			print_controls(t_win *w);
void			print_other(t_win *w);
void			controls_mgmt(t_win *win);
/*
** img_draw.c
*/
void			draw_image(t_win *win);
void			draw_line(t_win *win, t_point *start, t_point *end);
t_draw			*init_draw_line(t_win *win, t_point *start, t_point *end);
int				check_line(t_point *start, t_point *end);
void			draw_pixel(t_win *win, int x, int y, int color);
/*
** draw_tools.c
*/
int				auto_adjust(t_win *win);
int				check_zoom(t_win *win, double k);
void			clear_msg_bg(t_win *win);
int				mix_colors(t_draw *tmp, int *start, int *end, int i);
void			change_default_color(t_win *win, int keycode);
/*
** clean.c
*/
void			exit_program(t_win *win);
void			clear_image(t_win *win);
void			clear_all(t_file *input, t_point ***image, t_win *win);
void			free_image(t_point ***image);
void			free_input(t_file *input, int error_flag, const char *filename);

#endif
