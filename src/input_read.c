/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   input_read.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 23:52:55 by apyvovar          #+#    #+#             */
/*   Updated: 2017/03/28 16:32:11 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	parse_input(const char *filename)
{
	t_file	*input;
	t_point	***image_map;
	int		width;
	t_win	*win;

	image_map = NULL;
	win = NULL;
	width = 0;
	if ((input = save_file(filename)))
		width = get_width(input->line);
	else
		free_input(input, 3, filename);
	if (width && input_check(input, width))
	{
		image_map = create_image_map(input, width + 1, get_height(input) + 1);
		if (image_map && (win = win_init(image_map)))
			proceed_image(win);
		else
			fmt_print("Malloc error. Exiting...\n", 10, 2, 0);
	}
	else
		fmt_print("Input data error\n", 10, 2, 0);
	clear_all(input, image_map, win);
}

void	proceed_image(t_win *win)
{
	print_image(win);
	mlx_key_hook(win->mlx_win, keycode_mgmt, win);
	mlx_mouse_hook(win->mlx_win, mouse_mgmt, win);
	mlx_loop(win->mlx);
}

t_win	*win_init(t_point ***image_map)
{
	t_win	*win;

	if (!(win = (t_win *)malloc(sizeof(t_win))))
		return (NULL);
	win->mlx = mlx_init();
	win->mlx_win = mlx_new_window(win->mlx, SIDE, SIDE, "fdf");
	win->mlx_img = mlx_new_image(win->mlx, SIDE, SIDE);
	win->mlx_img_addr = mlx_get_data_addr(win->mlx_img, &win->bits_pp, \
		&win->size_line, &win->endian);
	win->image = image_map;
	win->default_color = 0xFFFFFF;
	win->diagonal_flag = 0;
	win->proj_type = 1;
	win->controls_flag = 1;
	win->adjust_flag = 0;
	win->rot_x = ROTATE_DEFAULT_X;
	win->rot_y = ROTATE_DEFAULT_Y;
	win->rot_z = ROTATE_DEFAULT_Z;
	win->x_shift = 0;
	win->y_shift = 0;
	return (win);
}

t_file	*save_file(const char *filename)
{
	int		fd;
	char	*line;
	t_file	*file;
	t_file	*temp;

	if ((fd = open(filename, O_RDONLY)) < 0)
		free_input(NULL, 1, filename);
	line = NULL;
	temp = NULL;
	file = NULL;
	while ((get_next_line(fd, &line)) > 0)
	{
		if (!temp)
			temp = (t_file *)malloc(sizeof(t_file));
		else if ((temp->next = (t_file *)malloc(sizeof(t_file))))
			temp = temp->next;
		else
			free_input(file, 2, filename);
		if (!file)
			file = temp;
		temp->line = line;
		temp->next = NULL;
	}
	close(fd);
	return (file);
}
