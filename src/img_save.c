/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   img_save.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/27 12:35:42 by apyvovar          #+#    #+#             */
/*   Updated: 2017/03/28 17:51:19 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_point	***create_image_map(t_file *input, int width, int height)
{
	t_point	***image;
	int		i;

	if (!(image = (t_point ***)malloc(sizeof(t_point **) * height)))
		return (NULL);
	i = 0;
	while (i < height - 1)
	{
		if (!(image[i] = set_image_line(input->line, i, width)))
		{
			free_image(image);
			return (NULL);
		}
		input = input->next;
		i++;
	}
	image[i] = NULL;
	free_input(input, 0, NULL);
	return (image);
}

t_point	**set_image_line(char *input_line, int line_num, int width)
{
	t_point	**map_line;
	int		i;

	if (!(map_line = ((t_point **)malloc(sizeof(t_point *) * width))))
		return (NULL);
	while (*input_line == ' ')
		input_line++;
	i = 0;
	while (i < width - 1)
	{
		if (!(map_line[i] = set_image_pixel(input_line, line_num, i)))
		{
			while (--i >= 0)
				free(map_line[i]);
			free(map_line);
			return (NULL);
		}
		input_line = ft_strchr(input_line, ' ');
		while (input_line && *input_line && *input_line == ' ')
			input_line++;
		i++;
	}
	map_line[i] = NULL;
	return (map_line);
}

t_point	*set_image_pixel(char *line, int x, int y)
{
	t_point	*point;

	if (!(point = (t_point *)malloc(sizeof(t_point))))
		return (NULL);
	point->x = (double)x;
	point->y = (double)y;
	point->z = (double)ft_atoi(line);
	point->x_default = point->x;
	point->y_default = point->y;
	point->z_default = point->z;
	point->px = 0;
	point->py = 0;
	point->color = get_color(line);
	return (point);
}

int		get_color(char *line)
{
	char	*temp;

	temp = ft_strchr(line, ' ');
	if ((line = ft_strchr(line, ',')))
		if (!temp || temp > line)
			return (ft_atoi_base(line + 3, 16));
	return (-1);
}
