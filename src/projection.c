/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   projection.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/13 18:43:24 by apyvovar          #+#    #+#             */
/*   Updated: 2017/03/28 18:05:02 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	make_projection(t_win *win)
{
	int		i;
	int		j;
	void	(*proj)(t_win *, int, int);

	proj = (win->proj_type == 1) ? &proj_parallel : &proj_iso;
	i = -1;
	while (win->image[++i])
	{
		j = -1;
		while (win->image[i][++j])
			proj(win, j, i);
	}
	if (auto_adjust(win))
		make_projection(win);
}

void	proj_parallel(t_win *win, int width, int height)
{
	double	x;
	double	y;
	double	z;

	x = win->image[height][width]->x;
	y = win->image[height][width]->y;
	z = win->image[height][width]->z;
	win->image[height][width]->px = round((x * cos(win->rot_y) + \
	(-y * sin(win->rot_x) + z * cos(win->rot_x)) * sin(win->rot_y)) * \
	cos(win->rot_z) + (y * cos(win->rot_x) + z * sin(win->rot_x)) * \
	sin(win->rot_z));
	win->image[height][width]->py = round(-(x * cos(win->rot_y) + \
	(-y * sin(win->rot_x) + z * cos(win->rot_x)) * sin(win->rot_y)) * \
	sin(win->rot_z) + (y * cos(win->rot_x) + z * sin(win->rot_x)) * \
	cos(win->rot_z));
	set_minmax(win, width, height);
}

void	proj_iso(t_win *win, int width, int height)
{
	double	x;
	double	y;
	double	z;
	double	angle_x;
	double	angle_y;

	angle_x = 0.785398163 + win->rot_x;
	angle_y = 0.615472907 + win->rot_y;
	x = win->image[height][width]->x;
	y = win->image[height][width]->y;
	z = win->image[height][width]->z;
	win->image[height][width]->px = round(x * cos(angle_y) + \
		(-y * sin(angle_x) + z * cos(angle_x)) * sin(angle_y));
	win->image[height][width]->py = round(y * cos(angle_x) + \
		z * sin(angle_x));
	set_minmax(win, width, height);
}

void	change_projection(t_win *win)
{
	clear_image(win);
	if (win->proj_type == 1)
	{
		win->proj_type = 2;
		win->rot_x = ROTATE_DEFAULT_ISO_X;
		win->rot_y = ROTATE_DEFAULT_ISO_Y;
		win->rot_z = 0;
	}
	else
	{
		win->proj_type = 1;
		win->rot_x = ROTATE_DEFAULT_X;
		win->rot_y = ROTATE_DEFAULT_Y;
		win->rot_z = ROTATE_DEFAULT_Z;
	}
	print_image(win);
}

void	set_minmax(t_win *win, int width, int height)
{
	int		x;
	int		y;

	x = win->image[height][width]->px;
	y = win->image[height][width]->py;
	if (!width && !height)
	{
		win->max_x = x;
		win->min_x = x;
		win->max_y = y;
		win->min_y = y;
	}
	else
	{
		win->max_x = (win->max_x < x) ? x : win->max_x;
		win->min_x = (win->min_x > x) ? x : win->min_x;
		win->max_y = (win->max_y < y) ? y : win->max_y;
		win->min_y = (win->min_y > y) ? y : win->min_y;
	}
}
