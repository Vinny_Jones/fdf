/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   img_actions.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/15 13:37:50 by apyvovar          #+#    #+#             */
/*   Updated: 2017/03/27 17:12:13 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	change_height(t_win *win, double n)
{
	int		i;
	int		j;
	double	max;
	double	deadline;
	double	z;

	clear_image(win);
	deadline = ABS(win->image[0][1]->y);
	if ((max = get_max_z(win)) < 0)
		n *= -1;
	i = -1;
	while (win->image[++i])
	{
		j = -1;
		while (win->image[i][++j])
			if ((z = win->image[i][j]->z) && ABS(z + z * n) > ABS(z))
				win->image[i][j]->z += z * n;
			else if (z && deadline > ABS(max + max * n))
				win->image[i][j]->z *= -1;
			else
				win->image[i][j]->z += z * n;
	}
	print_image(win);
}

double	get_max_z(t_win *win)
{
	int		i;
	int		j;
	double	max;

	max = 0;
	i = -1;
	while (win->image[++i])
	{
		j = -1;
		while (win->image[i][++j])
			max = (ABS(win->image[i][j]->z) > ABS(max)) ? \
				win->image[i][j]->z : max;
	}
	return (max);
}

void	zoom(t_win *win, double k, int printflag)
{
	int		i;
	int		j;

	clear_image(win);
	i = -1;
	while (win->image[++i])
	{
		j = -1;
		while (win->image[i][++j])
		{
			win->image[i][j]->x *= k;
			win->image[i][j]->y *= k;
			win->image[i][j]->z *= k;
		}
	}
	if (printflag)
		print_image(win);
}

void	diagonal_print(t_win *win)
{
	clear_image(win);
	win->diagonal_flag = (win->diagonal_flag) ? 0 : 1;
	print_image(win);
}
