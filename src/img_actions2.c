/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   img_actions2.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/26 13:30:17 by apyvovar          #+#    #+#             */
/*   Updated: 2017/03/27 14:15:07 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	rotate_x(t_win *win, int n)
{
	double	angle;

	clear_image(win);
	if (win->proj_type == 1)
		angle = ROTATE_ANGLE * n;
	else
		angle = 1.57079633 * n;
	win->rot_x += angle;
	print_image(win);
}

void	rotate_y(t_win *win, int n)
{
	double	angle;

	clear_image(win);
	if (win->proj_type == 1)
		angle = ROTATE_ANGLE * n;
	else
		angle = get_angle_iso(win->rot_y, n);
	win->rot_y += angle;
	print_image(win);
}

double	get_angle_iso(double current, int n)
{
	int	temp;

	temp = (int)round(current * 180 / PI);
	while (ABS(temp) >= 180)
		temp = (temp > 0) ? temp - 180 : temp + 180;
	if (temp == 0)
		return ((n > 0) ? 1.9106468396 : -1.230945814);
	return ((n > 0) ? 1.230945814 : -1.9106468396);
}

void	rotate_z(t_win *win, int n)
{
	double	angle;

	clear_image(win);
	if (win->proj_type == 1)
		angle = ROTATE_ANGLE * n;
	else
		angle = 0;
	win->rot_z += angle;
	print_image(win);
}

void	shift_img(t_win *win, int x_shift, int y_shift)
{
	clear_image(win);
	win->x_shift += x_shift;
	win->y_shift += y_shift;
	print_image(win);
}
