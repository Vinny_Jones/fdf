/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   img_print.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/21 22:44:43 by apyvovar          #+#    #+#             */
/*   Updated: 2017/03/28 16:33:25 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	print_image(t_win *win)
{
	make_projection(win);
	draw_image(win);
	mlx_put_image_to_window(win->mlx, win->mlx_win, win->mlx_img, 0, 0);
	print_info(win);
}

void	print_info(t_win *w)
{
	void	*mlx;
	void	*win;

	mlx = w->mlx;
	win = w->mlx_win;
	if (w->adjust_flag)
	{
		clear_msg_bg(w);
		if (w->adjust_flag == 1)
			mlx_string_put(mlx, win, 500, 10, 0x00FF00, \
				"Auto zoomed in to fit minimum picture size");
		else if (w->adjust_flag == 2)
			mlx_string_put(mlx, win, 500, 10, 0x00FF00, \
				"Auto zoomed out to fit maximum picture size");
		w->adjust_flag = 0;
	}
	if (!w->controls_flag)
	{
		mlx_string_put(mlx, win, 10, 10, 0xFFFFFF, "Press C to show controls");
		return ;
	}
	print_controls(w);
	print_other(w);
}

void	print_controls(t_win *w)
{
	void	*mlx;
	void	*win;

	mlx = w->mlx;
	win = w->mlx_win;
	mlx_string_put(mlx, win, 10, 10, 0xFFFFFF, "Press C to hide controls");
	mlx_string_put(mlx, win, 10, 30, 0xFFFFFF, "Projection:");
	mlx_string_put(mlx, win, 125, 30, 0x00FF00, \
		(w->proj_type == 1) ? "PAR" : "ISO");
	mlx_string_put(mlx, win, 160, 30, 0xFFFFFF, "(P - change)");
	mlx_string_put(mlx, win, 10, 50, 0xFFFFFF, "Controls:");
	mlx_string_put(mlx, win, 30, 70, 0xFFFFFF, "A/D - Rotate X");
	mlx_string_put(mlx, win, 30, 90, 0xFFFFFF, "W/S - Rotate Y");
	mlx_string_put(mlx, win, 30, 110, 0xFFFFFF, "Q/E - Rotate Z");
	mlx_string_put(mlx, win, 30, 130, 0xFFFFFF, "Arrows - Move image");
	mlx_string_put(mlx, win, 30, 150, 0xFFFFFF, "Z/X - Change Z (height)");
	mlx_string_put(mlx, win, 30, 170, 0xFFFFFF, "+/-/SCROLL - Zoom IN/OUT");
	mlx_string_put(mlx, win, 30, 190, 0xFFFFFF, "F - Print diagonal line");
	mlx_string_put(mlx, win, 30, 210, 0xFFFFFF, "[0 - 9] - Change color");
	mlx_string_put(mlx, win, 30, 230, 0xFFFFFF, "R - Discard changes");
	mlx_string_put(mlx, win, 30, 250, 0xFFFFFF, "ESC - Exit");
}

void	print_other(t_win *w)
{
	void	*mlx;
	void	*win;
	char	*str;

	mlx = w->mlx;
	win = w->mlx_win;
	mlx_string_put(mlx, win, 880, 850, 0xFFFFFF, "X:");
	str = ft_itoa((int)round((w->rot_x * 180) / PI));
	mlx_string_put(mlx, win, 900, 850, 0xFFFFFF, str);
	free(str);
	mlx_string_put(mlx, win, 880, 870, 0xFFFFFF, "Y:");
	str = ft_itoa((int)round((w->rot_y * 180) / PI));
	mlx_string_put(mlx, win, 900, 870, 0xFFFFFF, str);
	free(str);
	mlx_string_put(mlx, win, 880, 890, 0xFFFFFF, "Z:");
	str = ft_itoa((int)round((w->rot_z * 180) / PI));
	mlx_string_put(mlx, win, 900, 890, 0xFFFFFF, str);
	free(str);
}

void	controls_mgmt(t_win *win)
{
	clear_image(win);
	win->controls_flag = (win->controls_flag) ? 0 : 1;
	print_image(win);
}
