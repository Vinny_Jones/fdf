/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clean.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/21 15:16:29 by apyvovar          #+#    #+#             */
/*   Updated: 2017/03/28 23:00:32 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	exit_program(t_win *win)
{
	clear_all(NULL, win->image, win);
	exit(0);
}

void	clear_image(t_win *win)
{
	mlx_destroy_image(win->mlx, win->mlx_img);
	win->mlx_img = mlx_new_image(win->mlx, SIDE, SIDE);
	win->mlx_img_addr = mlx_get_data_addr(win->mlx_img, &win->bits_pp, \
		&win->size_line, &win->endian);
}

void	clear_all(t_file *input, t_point ***image, t_win *win)
{
	free_input(input, 0, NULL);
	free_image(image);
	if (win)
		free(win);
}

void	free_image(t_point ***image)
{
	int		i;
	int		j;

	if (!image)
		return ;
	i = -1;
	while (image[++i])
	{
		j = -1;
		while (image[i][++j])
			free(image[i][j]);
		free(image[i]);
	}
	free(image);
}

void	free_input(t_file *input, int error_flag, const char *filename)
{
	if (input)
	{
		if (input->next)
			free_input(input->next, 0, filename);
		free(input->line);
		free(input);
	}
	if (error_flag)
		fmt_print("File error: \t", 10, 2, 0);
	if (error_flag == 1)
		ft_printf("File [%s] does not exist\n", filename);
	if (error_flag == 2)
		ft_printf("Malloc error while saving [%s]\n", filename);
	if (error_flag == 3)
		ft_printf("File [%s] is empty\n", filename);
	if (error_flag)
		exit(0);
}
