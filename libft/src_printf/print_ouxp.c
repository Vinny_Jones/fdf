/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_ouxp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/03 16:40:51 by apyvovar          #+#    #+#             */
/*   Updated: 2017/01/20 14:12:20 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

int			print_unsigned(va_list ap, char *fmt, int width, int prec)
{
	char	*result;
	int		count;

	result = save_uint(ap, fmt, fmt_isnumarg(fmt));
	if (prec == 0 && result[0] == '0')
		result[0] = '\0';
	apply_flprec_int(prec, fmt, &result, ft_strlen(result));
	apply_flwid_int(width, fmt, &result, prec);
	count = ft_strlen(result);
	ft_putstr(result);
	free(result);
	return (count);
}

char		*save_uint(va_list ap, char *fmt, int num_arg)
{
	uintmax_t		nbr;
	unsigned int	base;

	if (num_arg)
		nbr = getnumarg_uint(ap, ft_atoi(fmt + 1));
	else
		nbr = va_arg(ap, uintmax_t);
	if (CFMT('j'))
		;
	else if (ACFMT('z'))
		nbr = INTTYPE_Z(nbr);
	else if (CFMT('l') || CFMT('O') || CFMT('U') || CFMT('p'))
		nbr = (ft_strstr(fmt, "ll")) ? UINTTYPE_LL(nbr) : UINTTYPE_L(nbr);
	else if (CFMT('h'))
		nbr = (ft_strstr(fmt, "hh")) ? UINTTYPE_HH(nbr) : UINTTYPE_H(nbr);
	else
		nbr = UINTTYPE_I(nbr);
	if (ACFMT('u'))
		base = 10;
	else
		base = (ACFMT('o')) ? 8 : 16;
	return (uint_itoa_base(nbr, fmt, base));
}

char		*uint_itoa_base(uintmax_t n, char *fmt, unsigned int base)
{
	char			*result;
	int				i;

	result = ft_strnew(1000);
	i = 0;
	while (n >= base)
	{
		result[i] = n % base + ((n % base > 9) ? 87 : 48);
		result[i] = (CFMT('X')) ? (ft_toupper(result[i])) : result[i];
		i++;
		n /= base;
	}
	result[i] = n + ((n > 9) ? 87 : 48);
	result[i] = (CFMT('X')) ? (ft_toupper(result[i])) : result[i];
	ft_strrev(result);
	return (result);
}

uintmax_t	getnumarg_uint(va_list ap, int n)
{
	uintmax_t	result;

	while (--n)
		va_arg(ap, uintmax_t);
	result = va_arg(ap, uintmax_t);
	return (result);
}
