/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 15:33:18 by apyvovar          #+#    #+#             */
/*   Updated: 2017/02/18 16:37:53 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void	ft_bzero(void *s, size_t n)
{
	size_t			j;
	unsigned char	*ptr;

	ptr = (unsigned char *)s;
	j = n;
	while (j-- > 0)
		*ptr++ = 0;
}
