/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_tools.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/23 12:46:40 by apyvovar          #+#    #+#             */
/*   Updated: 2017/03/28 18:04:01 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		auto_adjust(t_win *win)
{
	int		width;
	int		height;

	width = win->max_x - win->min_x + 1;
	height = win->max_y - win->min_y + 1;
	if (MAX(width, height) < MIN_IMG_SIDE)
	{
		zoom(win, (MIN_IMG_SIDE * 1.05) / (double)MAX(width, height), 0);
		win->adjust_flag = 1;
		return (1);
	}
	else if (MAX(width, height) > MAX_IMG_SIDE)
	{
		zoom(win, (MAX_IMG_SIDE * 0.95) / (double)MAX(width, height), 0);
		win->adjust_flag = 2;
		return (1);
	}
	return (0);
}

int		check_zoom(t_win *win, double k)
{
	int		width;
	int		height;

	width = win->max_x - win->min_x + 1;
	height = win->max_y - win->min_y + 1;
	if ((double)MAX(width, height) * k < MIN_IMG_SIDE)
	{
		clear_msg_bg(win);
		mlx_string_put(win->mlx, win->mlx_win, 500, 10, 0x00FF00, \
			"Minimum picture size. Forbidden to zoom out.");
		return (0);
	}
	else if ((double)MAX(width, height) * k > MAX_IMG_SIDE)
	{
		clear_msg_bg(win);
		mlx_string_put(win->mlx, win->mlx_win, 500, 10, 0x00FF00, \
			"Maximum picture size. Forbidden to zoom in.");
		return (0);
	}
	return (1);
}

void	clear_msg_bg(t_win *win)
{
	int	i;
	int	j;

	i = 10;
	while (++i < 32)
	{
		j = 499;
		while (++j < SIDE)
			mlx_pixel_put(win->mlx, win->mlx_win, j, i, 0x000000);
	}
}

int		mix_colors(t_draw *tmp, int *start, int *end, int i)
{
	int				result;
	int				percent;
	unsigned char	*ptr_res;
	unsigned char	*ptr_st;
	unsigned char	*ptr_end;

	percent = round(((double)i / (double)tmp->l) * 100);
	result = 0;
	ptr_res = (unsigned char *)&result;
	ptr_st = (unsigned char *)start;
	ptr_end = (unsigned char *)end;
	*ptr_res = (*ptr_st * percent + *ptr_end * (100 - percent)) / 100;
	*++ptr_res = (*++ptr_st * percent + *++ptr_end * (100 - percent)) / 100;
	*++ptr_res = (*++ptr_st * percent + *++ptr_end * (100 - percent)) / 100;
	return (result);
}

void	change_default_color(t_win *win, int keycode)
{
	clear_image(win);
	if (keycode == 29)
		win->default_color = 0xFFFFFF;
	else if (keycode == 18)
		win->default_color = 0xFF0000;
	else if (keycode == 19)
		win->default_color = 0x00FF00;
	else if (keycode == 20)
		win->default_color = 0x0000FF;
	else if (keycode == 21)
		win->default_color = 0x7F0F7E;
	else if (keycode == 23)
		win->default_color = 0xFDA428;
	else if (keycode == 22)
		win->default_color = 0xFEC0CB;
	else if (keycode == 26)
		win->default_color = 0x8ACEE9;
	else if (keycode == 28)
		win->default_color = 0x84FED5;
	else if (keycode == 25)
		win->default_color = 0x117F7F;
	print_image(win);
}
