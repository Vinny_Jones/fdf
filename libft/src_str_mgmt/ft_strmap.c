/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 16:35:43 by apyvovar          #+#    #+#             */
/*   Updated: 2017/02/18 17:43:52 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	char	*result;
	size_t	i;

	if (!s)
		return (NULL);
	i = 0;
	result = (char *)malloc(sizeof(char) * ft_strlen(s) + 1);
	if (result)
	{
		while (*s)
			result[i++] = f(*s++);
		result[i] = '\0';
	}
	return (result);
}
