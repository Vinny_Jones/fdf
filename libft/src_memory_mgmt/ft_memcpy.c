/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 13:16:44 by apyvovar          #+#    #+#             */
/*   Updated: 2017/02/18 17:00:41 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	size_t	j;

	j = 0;
	while (n-- > 0)
	{
		((unsigned char *)dst)[j] = ((unsigned char *)src)[j];
		j++;
	}
	return (dst);
}
