/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_events.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/14 11:12:02 by apyvovar          #+#    #+#             */
/*   Updated: 2017/03/27 17:18:41 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		keycode_mgmt(int keycode, void *par)
{
	t_win	*win;

	win = (t_win *)par;
	if (keycode == 53)
		exit_program(win);
	if (keycode == 8)
		controls_mgmt(win);
	if ((keycode == 27 || keycode == 78) && check_zoom(win, ZOOM_K))
		zoom(win, ZOOM_K, 1);
	if ((keycode == 24 || keycode == 69) && check_zoom(win, 1.0 / ZOOM_K))
		zoom(win, 1.0 / ZOOM_K, 1);
	if (keycode == 6)
		change_height(win, Z_INC);
	if (keycode == 7)
		change_height(win, Z_INC * -1);
	if (keycode == 35)
		change_projection(win);
	if (keycode == 15)
		discard_changes(win);
	if (keycode == 126 || keycode == 125)
		shift_img(win, 0, (keycode == 126) ? -50 : 50);
	if (keycode == 123 || keycode == 124)
		shift_img(win, (keycode == 123) ? -50 : 50, 0);
	return (keycode_mgmt2(keycode, win));
}

int		keycode_mgmt2(int keycode, t_win *win)
{
	if (keycode == 2)
		rotate_x(win, 1);
	if (keycode == 0)
		rotate_x(win, -1);
	if (keycode == 13)
		rotate_y(win, 1);
	if (keycode == 1)
		rotate_y(win, -1);
	if (keycode == 12)
		rotate_z(win, 1);
	if (keycode == 14)
		rotate_z(win, -1);
	if (keycode == 3)
		diagonal_print(win);
	if (keycode >= 18 && keycode <= 29)
		change_default_color(win, keycode);
	return (0);
}

int		mouse_mgmt(int button, int x, int y, void *par)
{
	t_win	*win;

	win = (t_win *)par;
	if (!win || x < 0 || y < 0)
		return (0);
	if (button == 4 && check_zoom(win, 1.0 / ZOOM_K))
		zoom(win, 1.0 / ZOOM_K, 1);
	if (button == 5 && check_zoom(win, ZOOM_K))
		zoom(win, ZOOM_K, 1);
	return (0);
}

void	discard_changes(t_win *win)
{
	int	i;
	int	j;

	clear_image(win);
	set_default_angle(win);
	i = -1;
	while (win->image[++i])
	{
		j = -1;
		while (win->image[i][++j])
		{
			win->image[i][j]->x = win->image[i][j]->x_default;
			win->image[i][j]->y = win->image[i][j]->y_default;
			win->image[i][j]->z = win->image[i][j]->z_default;
		}
	}
	win->x_shift = 0;
	win->y_shift = 0;
	win->default_color = 0xFFFFFF;
	print_image(win);
}

void	set_default_angle(t_win *win)
{
	if (win->proj_type == 1)
	{
		win->rot_x = ROTATE_DEFAULT_X;
		win->rot_y = ROTATE_DEFAULT_Y;
		win->rot_z = ROTATE_DEFAULT_Z;
	}
	else
	{
		win->rot_x = ROTATE_DEFAULT_ISO_X;
		win->rot_y = ROTATE_DEFAULT_ISO_Y;
		win->rot_z = 0;
	}
}
